#! /usr/bin/env python

#-------------------------------------------------------------------------------
# Name:        labelmaker
# Purpose:     Facebook Hacker Cup
#
# Author:      Dale Ross
#
# Created:     12/07/2013
#
#-------------------------------------------------------------------------------

import os
import sys
import re

def base10toN(num, base):
    """Change ``num'' to given base
    Upto base 36 is supported."""

    converted_string, modstring = "", ""
    currentnum = num
    if not 1 < base < 37:
        raise ValueError("base must be between 2 and 36")
    if not num:
        return '0'
    while currentnum:
        mod = currentnum % base
        currentnum = currentnum // base
        converted_string = chr(48 + mod + 7*(mod > 10)) + converted_string
    return converted_string

if len(sys.argv) != 2:
    print "Incorrect usage: python .py <inputfile>"
else:
    fi = open(sys.argv[1], "r")
    numcases = int(fi.readline())
    for caseindex in range(1,numcases+1):
        line = fi.readline()
        letters, lastbox = line.split(" ")
        lastbox = lastbox.rstrip()
        base = len(letters)
        indices =  base10toN(int(lastbox),base)
        label = ""
        for character in indices:
            label = label + letters[int(indices[int(character)])-1]
        print "Case #", caseindex,  ": ", label
    fi.close()






