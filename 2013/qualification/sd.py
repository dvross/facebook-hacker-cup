#! /usr/bin/env python


import os
import sys
import re


detectors = []

class SquareDetector:
    #Common base class for all detectors
    sdCount = 0
    lines = []


    def __init__(self, lines):
        self.nlines = len(lines)
      	self.lines = lines      	
      	SquareDetector.sdCount += 1
      	self.index = SquareDetector.sdCount
      	detectors.append(self)

    def printStatus(self):
        state = True
        print "Case #", self.index,  ": YES" if True else ": NO"

    def writeStatus(self,outputFile):
   		state = False
   		outputFile.write("Case #")
   		outputFile.write(str(self.index))
   		outputFile.write(": YES\n" if state else ": NO\n")


if len(sys.argv) != 2:
	print "Incorrect usage: python <inputfile>"
else:	
	fi = open(sys.argv[1], "r")
	numgrids = int(fi.readline())
	for num in range(1,numgrids+1):
		numlines = int(fi.readline())
		currlines = [];
		for num in range(1,numlines+1): 
			currline =  fi.readline()
			currlines.append(currline)
		sd = SquareDetector(currlines)		
	for detector in detectors:
		detector.printStatus()
	fi.close()
	



	
