#! /usr/bin/env python

#-------------------------------------------------------------------------------
# Name:        squaredetector
# Purpose:     Facebook Hacker Cup
#
# Author:      Dale
#
# Created:     24/11/2013
#
#-------------------------------------------------------------------------------

import os
import sys
import re

if len(sys.argv) != 2:
    print "Incorrect usage: python squaredetector.py <inputfile>"
else:
    fi = open(sys.argv[1], "r")
    numgrids = int(fi.readline())
    for gindex in range(1,numgrids+1):
        numlines = int(fi.readline())
        possibleSquare = True
        squareStarted = False
        firstBlackString = ""
        firstBlackStringLen = 0
        matchCount = 0
        for num in range(1,numlines+1):
            currline =  fi.readline()
            invalidmatch = re.search(r"#\.+#",currline)
            if invalidmatch:
                possibleSquare = False
                break
            else:
                possibleMatch = re.search(r"#+",currline)
                if possibleMatch:
                    potentialString = currline
                    if squareStarted:
                        if potentialString != firstBlackString:
                            possibleSquare = False
                            break
                        else:
                            matchCount+=1
                    else:
                        squareStarted = True
                        firstBlackString = potentialString
                        firstBlackStringLen = len(possibleMatch.group(0))
                        matchCount+=1
                    if matchCount > len(firstBlackString):
                        possibleSquare = False
                        break
                else:
                    if squareStarted  and (matchCount != firstBlackStringLen):
                        possibleSquare = False
                        break
                    else:
                        continue
        print "Case #", gindex,  ": YES" if (possibleSquare and squareStarted)  else ": NO"
        index = num
        while index < numlines:
            fi.readline()
            index+=1
    fi.close()





